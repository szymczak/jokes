package com.pawel.jokes.list

import android.content.Intent
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.IdlingResource
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.action.ViewActions.swipeDown
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.rule.ActivityTestRule
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import com.pawel.jokes.MockModule
import com.pawel.jokes.R
import com.pawel.jokes.common.network.model.ICNDBjoke
import com.pawel.jokes.common.network.model.ICNDBresponse
import com.pawel.jokes.list.view.JokesListActivity
import io.reactivex.Single
import kotlinx.android.synthetic.main.activity_jokes_list.*
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Rule
import org.junit.Test

class JokesListActivityTest {

    @get:Rule
    val activityRule: ActivityTestRule<JokesListActivity> =
        ActivityTestRule(JokesListActivity::class.java, false, false)

    private val mockApi = MockModule.mockApi

    private lateinit var progressBarIdlingResource: WaitForCondition
    private lateinit var refreshIdlingResource: WaitForCondition

    private val testDataSet1 = generateData(0..10)
    private val testDataSet2 = generateData(10..20)

    @Test
    fun load_can_be_retried() {

        whenever(mockApi.jokes(any()))
            .thenReturn(Single.error(Throwable()))
            .thenReturn(jokesTestData())

        whenever(mockApi.jokes(any()))
            .thenReturn(Single.error(Throwable()))
            .thenReturn(jokesTestData())

        launch()

        onView(withId(R.id.retryButton))
            .check(matches(isDisplayed()))
            .perform(click())

        onView(withId(R.id.jokesList))
            .check(matches(isDisplayed()))
    }

    @Test
    fun jokes_can_be_shown() {

        whenever(mockApi.jokes(any())).thenReturn(jokesTestData())

        launch()

        testDataSet1.verifyViews()

    }

    @Test
    fun swipe_down_refreshes_content() {

        whenever(mockApi.jokes(any())).thenReturn(jokesTestData())
            .thenReturn(jokesTestData(testDataSet2))

        launch()

        onView(withId(R.id.refresh)).perform(swipeDown())


        testDataSet2.verifyViews()

    }

    private fun List<String>.verifyViews() =
        this.mapIndexed { index, content ->

            onView(withId(R.id.jokesList))
                .perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(index))

            onView(CoreMatchers.allOf(withId(R.id.content), withText(content)))
                .check(matches(isDisplayed()))

        }

    private fun launch() {
        activityRule.launchActivity(Intent())
        progressBarIdlingResource =
            WaitForCondition("progressBar") { activityRule.activity.progressBar.isVisible.not() }

        refreshIdlingResource =
            WaitForCondition("refresh") { activityRule.activity.refresh.isRefreshing.not() }

        IdlingRegistry.getInstance().register(progressBarIdlingResource)
        IdlingRegistry.getInstance().register(refreshIdlingResource)
    }

    @After
    fun tearDown() {
        activityRule.finishActivity()
        if (::progressBarIdlingResource.isInitialized) {
            IdlingRegistry.getInstance().unregister(progressBarIdlingResource)
            IdlingRegistry.getInstance().unregister(refreshIdlingResource)
        }
    }

    private fun jokesTestData(testData: List<String> = testDataSet1) =
        Single.just(
            ICNDBresponse(
                "success",
                testData.map { ICNDBjoke(it) }.toList()
            )
        )

    private fun generateData(intRange: IntRange) = intRange.map { "joke $it" }

}

class WaitForCondition(
    private val name: String,
    private val condition: () -> Boolean
) : IdlingResource {

    private var resourceCallback: IdlingResource.ResourceCallback? = null

    override fun getName() = "WaitFor_$name"

    override fun isIdleNow() = condition()
        .also {
            if (it) {
                resourceCallback?.onTransitionToIdle()
            }
        }

    override fun registerIdleTransitionCallback(callback: IdlingResource.ResourceCallback?) {
        resourceCallback = callback
    }

}
