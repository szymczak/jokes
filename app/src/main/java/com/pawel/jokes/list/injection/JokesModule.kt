package com.pawel.jokes.list.injection

import com.pawel.jokes.list.repository.JokesRepository
import com.pawel.jokes.list.repository.network.NetworkJokesRepository
import dagger.Binds
import dagger.Module

@Module
abstract class JokesModule {

    @Binds
    abstract fun jokesRepository(impl: NetworkJokesRepository): JokesRepository

}