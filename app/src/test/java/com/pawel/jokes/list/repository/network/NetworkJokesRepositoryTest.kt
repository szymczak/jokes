package com.pawel.jokes.list.repository.network

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.pawel.jokes.common.network.ICNDBapi
import com.pawel.jokes.list.model.Joke
import com.pawel.jokes.common.network.model.ICNDBjoke
import com.pawel.jokes.common.network.model.ICNDBresponse
import io.reactivex.Single
import org.junit.Test

import java.io.IOException

class NetworkJokesRepositoryTest {

    private val mockApi = mock<ICNDBapi>()

    private val repository = NetworkJokesRepository(mockApi)

    @Test
    fun `if response is successful jokes are converted`() {

        val joke1 = "joke1"
        val joke2 = "joke2"

        whenever(mockApi.jokes(any())).thenReturn(
            Single.just(
                ICNDBresponse(
                    "success",
                    listOf(
                        ICNDBjoke(joke1),
                        ICNDBjoke(joke2)
                    )
                )
            )
        )

        repository.load()
            .test()
            .assertValue(listOf(Joke(joke1), Joke(joke2)))
    }

    @Test
    fun `if response is unsuccessful io error is received`() {

        whenever(mockApi.jokes(any())).thenReturn(
            Single.just(
                ICNDBresponse(
                    "error",
                    emptyList()
                )
            )
        )

        repository.load()
            .test()
            .assertError(IOException::class.java)

    }

    @Test
    fun `error is received if api errors`() {

        val throwable = Throwable("error")

        whenever(mockApi.jokes(any())).thenReturn(Single.error(throwable))

        repository.load()
            .test()
            .assertError(throwable)

    }

}

