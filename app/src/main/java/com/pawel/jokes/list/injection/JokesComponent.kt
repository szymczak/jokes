package com.pawel.jokes.list.injection

import com.pawel.jokes.common.injection.AppComponent
import com.pawel.jokes.list.JokesStateHolder
import dagger.Component

@JokesListScope
@Component(
    dependencies = [AppComponent::class],
    modules = [
        JokesModule::class,
        JokesModule::class
    ]
)
interface JokesComponent {

    fun inject(target: JokesStateHolder)

}
