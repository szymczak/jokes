package com.pawel.jokes.list.model

data class Joke(val content: String)