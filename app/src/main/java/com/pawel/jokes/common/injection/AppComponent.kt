package com.pawel.jokes.common.injection

import com.pawel.jokes.common.network.ICNDBapi
import dagger.Component
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers

@ApplicationScope
@Component(modules = [ApplicationModule::class])
interface AppComponent {

    @MainThreadScheduler
    fun mainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    fun icndbapi(): ICNDBapi

}
