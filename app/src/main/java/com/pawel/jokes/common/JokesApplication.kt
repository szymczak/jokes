package com.pawel.jokes.common

import android.app.Application
import com.pawel.jokes.common.injection.AppComponent
import com.pawel.jokes.common.injection.ApplicationModule
import com.pawel.jokes.common.injection.DaggerAppComponent
import io.reactivex.plugins.RxJavaPlugins

open class JokesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        RxJavaPlugins.setErrorHandler(errorHandler())
        appComponent = initAppComponent()
    }

    protected open fun initAppComponent(): AppComponent =
        DaggerAppComponent
            .builder()
            .applicationModule(ApplicationModule())
            .build()

    companion object {

        private lateinit var appComponent: AppComponent

        fun appComponent() = appComponent

    }

}