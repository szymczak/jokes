package com.pawel.jokes.list

import com.nhaarman.mockitokotlin2.*
import com.pawel.jokes.list.model.Joke
import com.pawel.jokes.list.repository.JokesRepository
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject
import org.junit.After
import org.junit.Before
import org.junit.Test
import kotlin.test.assertEquals

class JokesPresenterTest {

    private val ui = mock<JokesPresenter.JokesView>()

    private val mockJokesRepository = mock<JokesRepository>()

    private val inputs = PublishSubject.create<Input>()

    private lateinit var jokesPresenter: JokesPresenter

    private val testJokes = listOf(
        Joke("joke1"),
        Joke("joke2")
    )

    @Before
    fun setUp() {
        whenever(ui.inputs()).thenReturn(inputs)
    }

    @Test
    fun `when jokes load successfully ui is updated with loading and a list of jokes`() {

        whenever(mockJokesRepository.load()).thenReturn(Single.just(testJokes))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        inOrder(ui).run {
            verify(ui).update(UiModel(isLoading = true))
            verify(ui).update(UiModel(jokes = testJokes))
            verify(ui).inputs()
        }

    }

    @Test
    fun `when jokes fail to load loading ui is updated with loading and error`() {

        val error = Throwable("Error")

        whenever(mockJokesRepository.load())
            .thenReturn(Single.error(error))
            .thenReturn(Single.just(testJokes))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        inOrder(ui).run {
            verify(ui).update(UiModel(isLoading = true))
            verify(ui).update(UiModel(error = error))
            verify(ui).inputs()
        }

        verifyNoMoreInteractions(ui)

    }

    @Test
    fun `when jokes fail to load retry works`() {

        val error = Throwable("Error")

        whenever(mockJokesRepository.load()).thenReturn(Single.error(error))
            .thenReturn(Single.just(testJokes))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        inputs.onNext(Input.Load)

        inOrder(ui).run {
            verify(ui).update(UiModel(isLoading = true))
            verify(ui).update(UiModel(error = error))
            verify(ui).inputs()
            verify(ui).update(UiModel(isLoading = true))
            verify(ui).update(UiModel(jokes = testJokes))
        }

        verifyNoMoreInteractions(ui)

    }

    @Test
    fun `when jokes load successfully and then load with an error jokes are received with error`() {

        val error = Throwable("Error")

        whenever(mockJokesRepository.load())
            .thenReturn(Single.just(testJokes))
            .thenReturn(Single.error(error))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        inputs.onNext(Input.Load)

        inOrder(ui).run {
            verify(ui).update(UiModel(isLoading = true))
            verify(ui).update(UiModel(jokes = testJokes))
            verify(ui).inputs()
            verify(ui).update(
                UiModel(
                    jokes = testJokes,
                    isLoading = true
                )
            )
            verify(ui).update(UiModel(jokes = testJokes, error = error))
        }

        verifyNoMoreInteractions(ui)

    }

    @Test
    fun `no more inputs are processed after termination`() {

        whenever(mockJokesRepository.load()).thenReturn(Single.just(testJokes))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        verify(ui, times(2)).update(any())
        verify(ui).inputs()

        jokesPresenter.terminate()

        inputs.onNext(Input.Load)

        verifyNoMoreInteractions(ui)

    }

    @Test
    fun `state is kept when detaching and attaching`() {

        whenever(mockJokesRepository.load()).thenReturn(Single.just(testJokes))

        jokesPresenter = JokesPresenter(mockJokesRepository, Schedulers.trampoline())

        jokesPresenter.attach(ui)

        jokesPresenter.detach()

        jokesPresenter.attach(ui)

        val captor = argumentCaptor<UiModel>()

        verify(ui, times(3)).update(captor.capture())

        assertEquals(captor.thirdValue, captor.secondValue, "Last two updates should be the same")

    }

    @After
    fun tearDown() {
        jokesPresenter.detach()
    }
}