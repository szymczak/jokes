package com.pawel.jokes.common.injection

import javax.inject.Qualifier

@Qualifier
annotation class MainThreadScheduler