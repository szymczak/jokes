package com.pawel.jokes.list.repository.network

import com.pawel.jokes.common.network.ICNDBapi
import com.pawel.jokes.list.injection.JokesListScope
import com.pawel.jokes.list.model.Joke
import com.pawel.jokes.list.repository.JokesRepository
import io.reactivex.Single
import java.io.IOException
import javax.inject.Inject

@JokesListScope
class NetworkJokesRepository @Inject constructor(private val api: ICNDBapi) : JokesRepository {

    override fun load(): Single<List<Joke>> = api.jokes(
        (8..21).shuffled()
            .first()
    ).map { response ->

        if (response.type != "success") {
            throw IOException("Api response is unsuccessful")
        } else {
            response.jokes.map {
                Joke(it.joke)
            }
        }
    }

}
