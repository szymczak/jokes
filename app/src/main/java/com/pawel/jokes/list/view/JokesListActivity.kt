package com.pawel.jokes.list.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.jakewharton.rxbinding3.swiperefreshlayout.refreshes
import com.jakewharton.rxbinding3.view.clicks
import com.pawel.jokes.R
import com.pawel.jokes.list.Input
import com.pawel.jokes.list.JokesPresenter
import com.pawel.jokes.list.JokesStateHolder
import com.pawel.jokes.list.UiModel
import io.reactivex.Observable
import kotlinx.android.synthetic.main.activity_jokes_list.*

class JokesListActivity : AppCompatActivity(), JokesPresenter.JokesView {

    private lateinit var presenter: JokesPresenter

    private val postsAdapter = JokesAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_jokes_list)

        presenter = ViewModelProviders.of(this)
            .get(JokesStateHolder::class.java)
            .presenter

        jokesList.layoutManager = LinearLayoutManager(this)
        jokesList.adapter = postsAdapter
        jokesList.setHasFixedSize(true)

        presenter.attach(this)
    }

    override fun inputs(): Observable<Input> =
        Observable.merge(
            retryButton.clicks().map { Input.Load },
            refresh.refreshes().map { Input.Load }
        )

    override fun update(model: UiModel) {
        if (model.jokes != null) {
            postsAdapter.update(model.jokes)
            retryButton.visibility = View.GONE
            progressBar.visibility = View.GONE
            jokesList.visibility = View.VISIBLE

        } else {
            model.error?.let {
                progressBar.visibility = View.GONE
                retryButton.visibility = View.VISIBLE
            }
        }

        if (model.isLoading) {
            if (model.jokes == null) {
                retryButton.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
            } else if (refresh.isRefreshing.not()) {
                refresh.isRefreshing = true
            }
        } else {
            refresh.isRefreshing = false
            refresh.isEnabled = true
        }
    }


    override fun onDestroy() {
        presenter.detach()
        super.onDestroy()
    }

}
