package com.pawel.jokes

import com.pawel.jokes.common.JokesApplication
import com.pawel.jokes.common.injection.AppComponent
import com.pawel.jokes.common.injection.DaggerAppComponent

class TestApp : JokesApplication() {

    override fun initAppComponent(): AppComponent =
        DaggerAppComponent.builder()
            .applicationModule(MockModule())
            .build()
}
