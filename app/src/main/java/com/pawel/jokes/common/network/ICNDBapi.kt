package com.pawel.jokes.common.network

import com.pawel.jokes.common.network.model.ICNDBresponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ICNDBapi {

    @GET("jokes/random/{howManyJokes}")
    fun jokes(@Path("howManyJokes") howManyJokes: Int): Single<ICNDBresponse>

}