package com.pawel.jokes.common.network.model

import com.squareup.moshi.Json

data class ICNDBresponse(
    @field:Json(name = "type")
    val type: String,
    @field:Json(name = "value")
    val jokes: List<ICNDBjoke>
)

data class ICNDBjoke(
    @field:Json(name = "joke")
    val joke: String
)

