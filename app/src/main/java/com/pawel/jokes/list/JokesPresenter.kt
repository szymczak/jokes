package com.pawel.jokes.list

import com.pawel.jokes.common.injection.MainThreadScheduler
import com.pawel.jokes.list.injection.JokesListScope
import com.pawel.jokes.list.model.Joke
import com.pawel.jokes.list.repository.JokesRepository
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import javax.inject.Inject

@JokesListScope
class JokesPresenter @Inject constructor(
    jokesRepository: JokesRepository,
    @MainThreadScheduler mainThreadScheduler: Scheduler
) {

    private val disposables = CompositeDisposable()

    private val outputEvents: Observable<UiModel>
    private val inputEvents: Subject<Input> = PublishSubject.create()
    private val terminalEvents: Subject<Result.Terminate> = PublishSubject.create()

    init {
        outputEvents = inputEvents
            .map { Action.Load }
            .flatMap<Result> { loadPosts(jokesRepository, mainThreadScheduler) }
            .startWith(loadPosts(jokesRepository, mainThreadScheduler))
            .mergeWith(terminalEvents)
            .takeUntil { it is Result.Terminate }
            .ofType(Result.Ui::class.java)
            .scan(UiModel()) { state, result ->
                when (result) {
                    Result.Ui.Loading -> state.copy(isLoading = true, error = null)
                    is Result.Ui.Error -> state.copy(error = result.error, isLoading = false)
                    is Result.Ui.Loaded -> state.copy(
                        jokes = result.jokes,
                        isLoading = false
                    )
                }
            }
            .skip(1)
            .replay(1)
            .autoConnect()
    }

    private fun loadPosts(postsRepository: JokesRepository, deliveryScheduler: Scheduler) =
        postsRepository.load()
            .toObservable()
            .map<Result.Ui> { Result.Ui.Loaded(it) }
            .onErrorReturn { Result.Ui.Error(it) }
            .startWith(Result.Ui.Loading)
            .observeOn(deliveryScheduler)

    fun attach(ui: JokesView) {
        disposables.addAll(
            outputEvents
                .subscribe(ui::update),

            ui.inputs()
                .subscribe(inputEvents::onNext)
        )
    }

    fun detach() {
        disposables.clear()
    }

    fun terminate() {
        terminalEvents.onNext(Result.Terminate)
    }

    interface JokesView {
        fun inputs(): Observable<Input>
        fun update(model: UiModel)
    }

}

sealed class Input {
    object Load : Input()
}

private sealed class Action {
    object Load : Action()
}

private sealed class Result {

    object Terminate : Result()

    sealed class Ui : Result() {
        object Loading : Ui()
        data class Error(val error: Throwable) : Ui()
        data class Loaded(val jokes: List<Joke>) : Ui()
    }
}

data class UiModel(
    val error: Throwable? = null,
    val isLoading: Boolean = false,
    val jokes: List<Joke>? = null
)