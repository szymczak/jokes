package com.pawel.jokes.list.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pawel.jokes.R
import com.pawel.jokes.list.model.Joke
import kotlinx.android.synthetic.main.joke_item.view.*

class JokesAdapter : RecyclerView.Adapter<JokesAdapter.ViewHolder>() {

    private var jokes: List<Joke> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.joke_item,
                parent,
                false
            )
        )

    fun update(list: List<Joke>) {
        this.jokes = list
        notifyDataSetChanged()
    }

    override fun getItemCount() = jokes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.content.text = jokes[position].content
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val content: TextView = view.content
    }

}