package com.pawel.jokes.list.repository

import com.pawel.jokes.list.model.Joke
import io.reactivex.Single

interface JokesRepository {

    fun load(): Single<List<Joke>>

}
