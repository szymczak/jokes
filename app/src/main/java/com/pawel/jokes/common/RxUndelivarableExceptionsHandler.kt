package com.pawel.jokes.common

import io.reactivex.exceptions.UndeliverableException
import java.io.IOException
import java.net.SocketException

fun errorHandler(
    errorTracker: ErrorTracker = object :
        ErrorTracker {}
): (Throwable) -> Unit =
    { error ->

        when (error) {
            is UndeliverableException -> {
                when (error.cause) {
                    is IOException -> Unit
                    is SocketException -> Unit
                    is InterruptedException -> Unit
                    else -> {
                        errorTracker.track(error)
                        throw Throwable(
                            "UndeliverableException Rx exception",
                            error
                        ) // we can track or crash
                    }
                }
            }

            else -> {
                errorTracker.track(error)
                throw Throwable("Out of lifecycle Rx exception", error)
            }

        }

    }

interface ErrorTracker {

    fun track(error: Throwable) {
        //this can optionally be added to track errors instead of crashing the app, we could use Crashlytics, NewRelic etc...
    }

}
