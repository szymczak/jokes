package com.pawel.jokes.common.injection

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.pawel.jokes.BuildConfig
import com.pawel.jokes.common.network.ICNDBapi
import dagger.Module
import dagger.Provides
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

@Module
open class ApplicationModule {

    @Provides
    @ApplicationScope
    @MainThreadScheduler
    fun mainThreadScheduler(): Scheduler = AndroidSchedulers.mainThread()

    @Provides
    @ApplicationScope
    open fun provideICNDBapi(): ICNDBapi =
        Retrofit.Builder()
            .baseUrl(BuildConfig.JOKES_API_ENDPOINT)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io()))
            .addConverterFactory(MoshiConverterFactory.create())
            .build()
            .create(ICNDBapi::class.java)

}
