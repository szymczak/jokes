package com.pawel.jokes

import com.nhaarman.mockitokotlin2.mock
import com.pawel.jokes.common.injection.ApplicationModule
import com.pawel.jokes.common.network.ICNDBapi
import dagger.Module
import dagger.Provides

@Module
class MockModule : ApplicationModule() {

    @Provides
    override fun provideICNDBapi(): ICNDBapi = mockApi

    companion object {
        val mockApi = mock<ICNDBapi>()
    }

}