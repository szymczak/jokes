package com.pawel.jokes.list

import androidx.lifecycle.ViewModel
import com.pawel.jokes.common.JokesApplication
import com.pawel.jokes.list.injection.DaggerJokesComponent
import com.pawel.jokes.list.injection.JokesComponent
import javax.inject.Inject

class JokesStateHolder : ViewModel() {

    @Inject
    lateinit var presenter: JokesPresenter

    private val component: JokesComponent =
        DaggerJokesComponent.builder()
            .appComponent(JokesApplication.appComponent())
            .build()

    init {
        component.inject(this)
    }

    override fun onCleared() {
        presenter.terminate()
        super.onCleared()
    }
}